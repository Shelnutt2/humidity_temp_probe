# Humidity and Temperature Sensor Probe PCB

This is a kicad project for pcb for the humidity and temperature sensor probe


## Submodule

There is a sub module dependency for SHT3x sensor.

```bash
git submodule update --init
```
